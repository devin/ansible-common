Common Role for standard server configuration
=========

A role to standardize network wide configurations

Requirements
------------

This role is intended to be included in a project with the specific variables and config files provided. This repository is the generic configuration which will be the same regardless of the specific configuration of the instances.  In other words the requirement of this repository is that it be used as a requirement in another repository.

Role Variables
--------------

Both the software and config file variables are optional

Dependencies
------------

This role does not require any other roles.

Example Playbook
----------------

Just call the role.  All variables are optional:

    - hosts: servers
      roles:
         - { role: common, x: 42 }


Author Information
------------------

* **Devin** - *Initial work* - [Other Projects](https://gitlab.com/devin)
