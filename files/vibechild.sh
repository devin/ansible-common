# Standard profile additions for all machines

PS1="\[\033]0;\h:\w\007\]\[\e[0;36m\]\u\[\e[1;37m\]@\[\e[0;37m\]\h\[\e[0;36m\](\W)\[\e[0;0m\]$ "
if [ `id -u` -lt 99 ]; then
        PS1="\[\033]0;\h:\w\007\]\[\e[0;31m\]\u\[\e[1;37m\]@\[\e[0;37m\]\h\[\e[0;36m\](\W)\[\e[0;0m\]# "
fi

alias which="type -path"
alias du="du -h"
alias df="df -h"
alias ll="ls -lh"
alias services="cd /etc/rc.d/init.d;ls"
alias x="exit"